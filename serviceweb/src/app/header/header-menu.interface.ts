export interface IHeaderMenu {
  link: string;
  text: string;
}
