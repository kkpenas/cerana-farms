import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {IHeaderMenu} from './header-menu.interface';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private http: HttpClient) { }

  getHeaderMenu(): Observable<IHeaderMenu[]> {
    const url = `${environment.jsonUrl}/header-menu.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.menu))
    );
  }
}
