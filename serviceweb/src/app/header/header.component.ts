import { Component, OnInit } from '@angular/core';
import {HeaderService} from './header.service';
import {IHeaderMenu} from './header-menu.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  headerMenu: IHeaderMenu[];

  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {
    this.headerService.getHeaderMenu()
      .subscribe((jsonHeaderMenu: IHeaderMenu[]) => {
        this.headerMenu = jsonHeaderMenu;
      });
  }

}
