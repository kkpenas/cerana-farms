export interface IGalleryPhoto {
  altText: string;
  fileName: string;
}
