import { Component, OnInit } from '@angular/core';
import {GalleryService} from './gallery.service';
import {IGalleryPhoto} from './gallery-photo.interface';
import {IGalleryFacebookVideo} from './gallery-facebook-video.interface';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DeviceDetectorService, DeviceInfo} from 'ngx-device-detector';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  gallerySrc: string;
  photos: IGalleryPhoto[];
  facebookVideos: IGalleryFacebookVideo[];
  facebookVideosSrc: SafeResourceUrl[] = [];
  isDesktop: boolean;
  fbWidth: number;
  fbHeight: number;

  constructor(private galleryService: GalleryService,
              private sanitizer: DomSanitizer,
              private deviceService: DeviceDetectorService) { }

  ngOnInit(): void {
    this.isDesktop = this.deviceService.isDesktop();
    this.gallerySrc = '../assets/images/gallery/';

    this.galleryService.getGalleryPhotos()
      .subscribe((jsonGalleryPhotos: IGalleryPhoto[]) => {
        this.photos = jsonGalleryPhotos;
      });

    this.fbWidth = 320;
    this.fbHeight = 240;

    this.galleryService.getGalleryFacebookVideos()
      .subscribe((jsonGalleryFacebookVideos: IGalleryFacebookVideo[]) => {
        this.facebookVideos = jsonGalleryFacebookVideos;
        this.facebookVideos.forEach((fbVideo) => {
          const videoLink = `https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F${fbVideo.pageAccount}%2Fvideos%2F${fbVideo.ID}%2F&width=800&show_text=true&height=411&appId`;
          this.facebookVideosSrc.push(this.sanitizer.bypassSecurityTrustResourceUrl(videoLink));
        });
      });
  }

}
