export interface IGalleryFacebookVideo {
  ID: number;
  pageAccount: string;
  date: string;
  fullDate: string;
  link: string;
  postedBy: string;
  summary: string;
  title: string;
}
