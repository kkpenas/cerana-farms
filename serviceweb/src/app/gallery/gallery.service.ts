import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {IGalleryPhoto} from './gallery-photo.interface';
import {IGalleryFacebookVideo} from './gallery-facebook-video.interface';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  constructor(private http: HttpClient) { }

  getGalleryPhotos(): Observable<IGalleryPhoto[]> {
    const url = `${environment.jsonUrl}/gallery-photos.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.photos))
    );
  }

  getGalleryFacebookVideos(): Observable<IGalleryFacebookVideo[]> {
    const url = `${environment.jsonUrl}/gallery-facebook-videos.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.facebookVideos))
    );
  }
}
