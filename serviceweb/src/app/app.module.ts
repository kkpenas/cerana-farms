import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AboutUsComponent } from './about-us/about-us.component';
import { AdvisoryComponent } from './advisory/advisory.component';
import { ComingSoonComponent } from './coming-soon/coming-soon.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FooterComponent } from './footer/footer.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import {HttpClientModule} from '@angular/common/http';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { TextEditorPageComponent } from './text-editor-page/text-editor-page.component';
import {DeviceDetectorModule} from 'ngx-device-detector';

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    AdvisoryComponent,
    HomeComponent,
    ComingSoonComponent,
    ContactUsComponent,
    FooterComponent,
    GalleryComponent,
    HeaderComponent,
    LayoutComponent,
    PageNotFoundComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    TextEditorPageComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ClipboardModule,
    HttpClientModule,
    DeviceDetectorModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
