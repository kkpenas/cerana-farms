import { Component, OnInit } from '@angular/core';
import {FooterService} from './footer.service';
import {IFooterMenu} from './footer-menu.interface';
import {IFooterBusinessTimes} from './footer-business-times.interface';
import {IFooterContactInformation} from './footer-contact-information.interface';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerMenu: IFooterMenu[];
  businessTimes: IFooterBusinessTimes[];
  contactInformation: IFooterContactInformation;

  constructor(private footerService: FooterService) { }

  ngOnInit(): void {
    this.footerService.getFooterMenu()
      .subscribe((jsonFooterMenu: IFooterMenu[]) => {
        this.footerMenu = jsonFooterMenu;
      });

    this.footerService.getFooterBusinessHours()
      .subscribe((jsonFooterBusinessHours: IFooterBusinessTimes[]) => {
        this.businessTimes = jsonFooterBusinessHours;
      });

    this.footerService.getFooterContactInformation()
      .subscribe((jsonContactInformation: IFooterContactInformation) => {
        this.contactInformation = jsonContactInformation;
      });
  }

}
