export interface IFooterContactInformation {
  mapLocation: string;
  address1: string;
  address2: string;
  country: string;
  email: string;
  mobile: string;
}
