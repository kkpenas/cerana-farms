export interface IFooterBusinessHours {
  from: string;
  to: string;
}

export interface IFooterBusinessTimes {
  day: string;
  time: IFooterBusinessHours;
}
