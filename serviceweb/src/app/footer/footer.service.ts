import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IFooterMenu} from './footer-menu.interface';
import {IFooterBusinessTimes} from './footer-business-times.interface';
import {IFooterContactInformation} from './footer-contact-information.interface';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  constructor(private http: HttpClient) { }

  getFooterMenu(): Observable<IFooterMenu[]> {
    const url = `${environment.jsonUrl}/footer-menu.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.menu))
    );
  }

  getFooterBusinessHours(): Observable<IFooterBusinessTimes[]> {
    const url = `${environment.jsonUrl}/footer-business-times.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.businessTimes))
    );
  }

  getFooterContactInformation(): Observable<IFooterContactInformation> {
    const url = `${environment.jsonUrl}/footer-contact-information.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.contactInformation))
    );
  }
}
