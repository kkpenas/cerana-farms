export interface IFooterMenu {
  link: string;
  text: string;
}
