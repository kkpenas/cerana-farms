import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ComingSoonComponent} from './coming-soon/coming-soon.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {GalleryComponent} from './gallery/gallery.component';
import {LayoutComponent} from './layout/layout.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsOfUseComponent} from './terms-of-use/terms-of-use.component';
import {TextEditorPageComponent} from './text-editor-page/text-editor-page.component';

const appRoutes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
          { path: 'home', component: HomeComponent },
          { path: 'about-us', component: AboutUsComponent },
          { path: 'coming-soon', component: ComingSoonComponent },
          { path: 'contact-us', component: ContactUsComponent },
          { path: 'gallery', component: GalleryComponent },
          { path: 'page-not-found', component: PageNotFoundComponent },
          { path: 'privacy-policy', component: PrivacyPolicyComponent },
          { path: 'terms-of-use', component: TermsOfUseComponent },
          { path: 'text-editor-page', component: TextEditorPageComponent },
          {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full'
          }
        ]
    },
    { path: '**', redirectTo: 'page-not-found'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
