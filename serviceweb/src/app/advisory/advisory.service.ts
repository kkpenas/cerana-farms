import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdvisoryService {

  constructor(private http: HttpClient) { }

  getAdvisoryNote() {
    const url = `${environment.jsonUrl}/advisory-note.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.advisoryNote))
    );
  }
}
