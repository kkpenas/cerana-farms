import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AdvisoryService} from './advisory.service';
import {QuillEditorService} from '../shared/quill-editor.service';

@Component({
  selector: 'app-advisory',
  templateUrl: './advisory.component.html',
  styleUrls: ['./advisory.component.scss']
})
export class AdvisoryComponent implements OnInit {
  @ViewChild('advisoryNote') advisoryNote: ElementRef;

  constructor(private advisoryService: AdvisoryService, private quillService: QuillEditorService) { }

  ngOnInit(): void {
    this.advisoryService.getAdvisoryNote()
      .subscribe((jsonAdvisoryNote: string) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonAdvisoryNote[0]);
        this.advisoryNote.nativeElement.innerHTML = quill.root.innerHTML;
      });
  }

}
