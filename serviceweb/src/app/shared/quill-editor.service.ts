import {Injectable} from '@angular/core';
import * as Quill from 'quill';

@Injectable({
  providedIn: 'root'
})
export class QuillEditorService {
  constructor() {

  }

  instantiate() {
    const BackgroundClass = Quill.import('attributors/class/background');
    const ColorClass = Quill.import('attributors/class/color');
    const SizeStyle = Quill.import('attributors/style/size');
    const Font = Quill.import('formats/font');
    const Link = Quill.import('formats/link');
    const Bold = Quill.import('formats/bold');
    Bold.tagName = 'b';
    Font.whitelist = ['serif', 'monospace', 'cursive'];

    Quill.register(BackgroundClass, true);
    Quill.register(ColorClass, true);
    Quill.register(SizeStyle, true);
    Quill.register(Font, true);
    Quill.register(Link, true);
    Quill.register(Bold, true);

    return new Quill('#quill-editor', {
      theme: 'snow'
    });
  }

}
