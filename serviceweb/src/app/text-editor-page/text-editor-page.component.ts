import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as Quill from 'quill';
import {Clipboard} from '@angular/cdk/clipboard';

@Component({
  selector: 'app-text-editor-page',
  templateUrl: './text-editor-page.component.html',
  styleUrls: ['./text-editor-page.component.scss']
})
export class TextEditorPageComponent implements OnInit, AfterViewInit {
  public quill;
  @ViewChild('editor') editor: TextEditorPageComponent;
  @ViewChild('outputEditorText') outputEditor: ElementRef;
  @ViewChild('outputRawEditorText') outputRawEditor: ElementRef;

  constructor(private clipboard: Clipboard) { }

  public onTextChange = (delta, oldDelta, source) => {};

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    const BackgroundClass = Quill.import('attributors/class/background');
    const ColorClass = Quill.import('attributors/class/color');
    const SizeStyle = Quill.import('attributors/style/size');
    const Font = Quill.import('formats/font');
    const Link = Quill.import('formats/link');
    const Bold = Quill.import('formats/bold');
    Bold.tagName = 'b';
    Font.whitelist = ['serif', 'monospace', 'cursive'];

    Quill.register(BackgroundClass, true);
    Quill.register(ColorClass, true);
    Quill.register(SizeStyle, true);
    Quill.register(Font, true);
    Quill.register(Link, true);
    Quill.register(Bold, true);

    const quill = new Quill('#editor-container', {
      modules: {
        toolbar: '#toolbar'
      },
      placeholder: 'Compose an epic...',
      theme: 'snow'
    });

    quill.on('text-change', (delta, oldDelta, source) => {
      this.onTextChange(delta, oldDelta, source);
    });

    this.quill = quill;

    this.quill.on('text-change', () => {
      this.outputEditor.nativeElement.innerHTML = this.quill.root.innerHTML;
      const delta = this.quill.getContents();
      this.outputRawEditor.nativeElement.innerHTML = JSON.stringify(delta);
    });
  }

  copyDelta() {
    const delta = JSON.stringify(this.quill.getContents());
    this.clipboard.copy(delta);
  }
}
