import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {IContactUs} from './contact-us.interface';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor(private http: HttpClient) { }

  getContactUs(): Observable<IContactUs> {
    const url = `${environment.jsonUrl}/contact-us.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.contactUs))
    );
  }
}
