import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ContactUsService} from './contact-us.service';
import {ContactUs, IContactUs, IContactUsContactNumber, IContactUsFarms} from './contact-us.interface';

let map;
declare var google: any;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit, AfterViewInit {
  @ViewChild('mapContainer', {static: false}) googleMap: ElementRef;
  contactUs: IContactUs = new ContactUs();

  constructor(private contactUsService: ContactUsService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.contactUsService.getContactUs()
      .subscribe((jsonContactUs: IContactUs) => {
          this.contactUs = {
            contactNumbers: jsonContactUs.contactNumbers,
            farms: jsonContactUs.farms,
            emails: jsonContactUs.emails,
            facebookLink: jsonContactUs.facebookLink,
            mapCenter: jsonContactUs.mapCenter
          };

          this.mapInitializer();
      });
  }

  mapInitializer() {
    const mapOptions = {
      center: new google.maps.LatLng(this.contactUs.mapCenter.lat, this.contactUs.mapCenter.lng),
      zoom: 11
    };

    map = new google.maps.Map(this.googleMap.nativeElement, mapOptions);

    this.contactUs.farms.forEach((farm) => {
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(farm.lat, farm.lng),
        map,
      });

      const infoWindow = new google.maps.InfoWindow({
        content: '<b>' + farm.farmName + '</b><br />' + farm.address1 + ' ' + farm.address2 + ' ' + farm.country,
        size: new google.maps.Size(150, 50)
      });

      marker.setMap(map);
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(map, marker);
      });
    });

  }

}
