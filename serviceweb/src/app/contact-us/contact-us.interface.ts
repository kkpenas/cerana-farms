export interface IContactUs {
  contactNumbers: IContactUsContactNumber[];
  farms: IContactUsFarms[];
  emails: string[];
  facebookLink: string;
  mapCenter: IContactUsCoordinates;
}
export interface IContactUsCoordinates {
  lat: string;
  lng: string;
}
export interface IContactUsContactNumber {
  name: string;
  number: string;
}
export interface IContactUsFarms {
  farmName: string;
  address1: string;
  address2: string;
  country: string;
  link: string;
  lat: number;
  lng: number;
}

export class ContactUs implements IContactUs{
  constructor() {
  }

  contactNumbers: IContactUsContactNumber[];
  farms: IContactUsFarms[];
  emails: string[];
  facebookLink: string;
  mapCenter: IContactUsCoordinates;
}
