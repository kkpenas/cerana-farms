import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ComingSoonService} from './coming-soon.service';
import {QuillEditorService} from '../shared/quill-editor.service';

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.scss']
})
export class ComingSoonComponent implements OnInit {
  @ViewChild('comingSoon') comingSoon: ElementRef;

  constructor(private comingSoonService: ComingSoonService, private quillService: QuillEditorService) { }

  ngOnInit(): void {
    this.comingSoonService.getComingSoon()
      .subscribe((jsonComingSoon: string) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonComingSoon[0]);
        this.comingSoon.nativeElement.innerHTML = quill.root.innerHTML;
      });
  }

}
