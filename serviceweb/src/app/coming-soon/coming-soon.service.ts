import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ComingSoonService {

  constructor(private http: HttpClient) { }

  getComingSoon() {
    const url = `${environment.jsonUrl}/coming-soon.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.comingSoon))
    );
  }
}
