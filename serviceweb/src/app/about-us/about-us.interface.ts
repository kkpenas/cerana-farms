export interface IAboutUs {
  title: string;
  summary: any;
  body: IWhoWeAre[];
}

export interface IWhoWeAre {
  altText: string;
  fileName: string;
  name: string;
  position: string;
  description: string;
}

export class AboutUs implements IAboutUs{
  constructor() {
  }

  title: string;
  summary: any;
  body: IWhoWeAre[];
}
