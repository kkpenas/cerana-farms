import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {IAboutUs} from './about-us.interface';

@Injectable({
  providedIn: 'root'
})
export class AboutUsService {

  constructor(private http: HttpClient) { }

  getAboutUsSummary(): Observable<IAboutUs> {
    const url = `${environment.jsonUrl}/about-us-summary.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.aboutUsSummary))
    );
  }
  getAboutUsOurFarmers(): Observable<IAboutUs> {
    const url = `${environment.jsonUrl}/about-us-our-farmers.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.aboutUsOurFarmers))
    );
  }
  getAboutUsVisionMission(): Observable<IAboutUs> {
    const url = `${environment.jsonUrl}/about-us-vision-and-mission.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.aboutUsVisionMission))
    );
  }
  getAboutUsWhoWeAre(): Observable<IAboutUs> {
    const url = `${environment.jsonUrl}/about-us-who-we-are.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.aboutUsWhoWeAre))
    );
  }
}
