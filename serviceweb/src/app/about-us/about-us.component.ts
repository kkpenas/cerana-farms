import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AboutUsService} from './about-us.service';
import {AboutUs, IAboutUs} from './about-us.interface';
import {QuillEditorService} from '../shared/quill-editor.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  @ViewChild('aboutUsSummary') aboutUsSummary: ElementRef;
  @ViewChild('aboutUsOurFarmersSummary') aboutUsOurFarmersSummary: ElementRef;
  @ViewChild('aboutUsVisionMissionSummary') aboutUsVisionMissionSummary: ElementRef;
  @ViewChild('aboutUsWhoWeAreSummary') aboutUsWhoWeAreSummary: ElementRef;

  aboutUs: IAboutUs = new AboutUs();
  aboutUsOurFarmers: IAboutUs = new AboutUs();
  aboutUsVisionMission: IAboutUs = new AboutUs();
  aboutUsWhoWeAre: IAboutUs = new AboutUs();

  imageSrc = '../assets/images/';

  constructor(private aboutUsService: AboutUsService, private quillService: QuillEditorService) { }

  ngOnInit(): void {
    this.aboutUsService.getAboutUsSummary()
      .subscribe((jsonAboutUsSummary: AboutUs) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonAboutUsSummary.summary[0]);
        this.aboutUsSummary.nativeElement.innerHTML = quill.root.innerHTML;
        this.aboutUs = {
          title: jsonAboutUsSummary.title,
          summary: jsonAboutUsSummary.summary,
          body: jsonAboutUsSummary.body
        };
      });
    this.aboutUsService.getAboutUsOurFarmers()
      .subscribe((jsonAboutUsOurFarmers: AboutUs) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonAboutUsOurFarmers.summary[0]);
        this.aboutUsOurFarmersSummary.nativeElement.innerHTML = quill.root.innerHTML;
        this.aboutUsOurFarmers =  {
          title: jsonAboutUsOurFarmers.title,
          summary: jsonAboutUsOurFarmers.summary,
          body: jsonAboutUsOurFarmers.body
        };
      });
    this.aboutUsService.getAboutUsVisionMission()
      .subscribe((jsonAboutUsVisionMission: AboutUs) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonAboutUsVisionMission.summary[0]);
        this.aboutUsVisionMissionSummary.nativeElement.innerHTML = quill.root.innerHTML;
        this.aboutUsVisionMission = {
          title: jsonAboutUsVisionMission.title,
          summary: jsonAboutUsVisionMission.summary,
          body: jsonAboutUsVisionMission.body
        };
      });
    this.aboutUsService.getAboutUsWhoWeAre()
      .subscribe((jsonAboutUsWhoWeAre: AboutUs) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonAboutUsWhoWeAre.summary[0]);
        this.aboutUsWhoWeAreSummary.nativeElement.innerHTML = quill.root.innerHTML;
        this.aboutUsWhoWeAre =  {
          title: jsonAboutUsWhoWeAre.title,
          summary: jsonAboutUsWhoWeAre.summary,
          body: jsonAboutUsWhoWeAre.body
        };
      });
  }

}
