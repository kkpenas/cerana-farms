export interface IHomeWelcome {
  coverPhoto: IHomeCoverPhoto;
  video: IHomeVideo;
  title: string;
  summary: any;
}
export interface IHomeCoverPhoto {
  fileName: string;
  altText: string;
}
export interface IHomeVideo {
  ID: number;
  pageAccount: string;
}

export class HomeWelcome implements  IHomeWelcome {
  constructor() {
  }

  coverPhoto: HomeCoverPhoto = new HomeCoverPhoto();
  video: HomeVideo = new HomeVideo();
  title: string;
  summary: any;
}
export class HomeCoverPhoto implements IHomeCoverPhoto {
  constructor() {
  }
  fileName: string;
  altText: string;
}

export class HomeVideo implements  IHomeVideo {
  constructor() {
  }
  ID: number;
  pageAccount: string;
}
