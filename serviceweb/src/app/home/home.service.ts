import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {IHomeFarm} from './home-farm.interface';
import {IHomeWelcome} from './home-welcome.interface';
import {IHomeProduct} from './home-product.interface';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getHomeWelcome(): Observable<IHomeWelcome> {
    const url = `${environment.jsonUrl}/home-welcome.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.homeWelcome))
    );
  }

  getHomeFarms(): Observable<IHomeFarm> {
    const url = `${environment.jsonUrl}/home-farms.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.homeFarms))
    );
  }

  getHomeProducts(): Observable<IHomeProduct> {
    const url = `${environment.jsonUrl}/home-products.json`;
    return this.http.get(url).pipe(
      map(((result: any) => result.homeProducts))
    );
  }
}
