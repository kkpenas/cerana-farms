export interface IHomeFarm {
  farms: IHomeFarmSection[];
  title: string;
  summary: any;
}

export interface IHomeFarmSection {
  fileName: string;
  altText: string;
  title: string;
  summary: string;
}

export class HomeFarm implements IHomeFarm {
  constructor() {
  }

  farms: IHomeFarmSection[];
  title: string;
  summary: any;
}
