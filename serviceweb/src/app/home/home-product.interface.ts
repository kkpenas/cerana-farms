import {IHomeFarmSection} from "./home-farm.interface";

export interface IHomeProduct {
  brands: IHomeBrands[];
  title: string;
  summary: any;
}
export interface IHomeBrands {
  fileName: string;
  altText: string;
  title: string;
  summary: string;
}

export class HomeProduct implements  IHomeProduct{
  constructor() {
  }

  brands: IHomeBrands[];
  title: string;
  summary: any;
}
