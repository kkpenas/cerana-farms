import {Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {HomeService} from './home.service';
import {HomeProduct, IHomeProduct} from './home-product.interface';
import {HomeWelcome, IHomeWelcome} from './home-welcome.interface';
import {HomeFarm, IHomeFarm, IHomeFarmSection} from './home-farm.interface';
import {QuillEditorService} from '../shared/quill-editor.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {DeviceDetectorService} from 'ngx-device-detector';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('homeWelcomeSummary') homeWelcomeSummary: ElementRef;
  @ViewChild('homeFarmsSummary') homeFarmsSummary: ElementRef;
  @ViewChild('homeProductsSummary') homeProductsSummary: ElementRef;
  @ViewChildren('homeFarmSummaries') homeFarmSummaries: QueryList<ElementRef>;
  @ViewChild('fbIframe') fbIframe: ElementRef;

  homeFarms: IHomeFarm = new HomeFarm();
  homeWelcome: IHomeWelcome = new HomeWelcome();
  homeProducts: IHomeProduct = new HomeProduct();
  imageSrc = '../assets/images/';
  homeVideoSrc: SafeResourceUrl;
  coverPhotoSrc: SafeResourceUrl;
  isDesktop: boolean;
  fbWidth: number;
  fbHeight: number;

  constructor(private homeService: HomeService,
              private quillService: QuillEditorService,
              private sanitizer: DomSanitizer,
              private deviceService: DeviceDetectorService) {
  }

  ngOnInit(): void {
    this.isDesktop = this.deviceService.isDesktop();
    this.homeService.getHomeWelcome()
      .subscribe((jsonHomeWelcome: IHomeWelcome) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonHomeWelcome.summary[0]);
        this.homeWelcomeSummary.nativeElement.innerHTML = quill.root.innerHTML;

        this.homeWelcome = {
          coverPhoto: jsonHomeWelcome.coverPhoto,
          video: jsonHomeWelcome.video,
          title: jsonHomeWelcome.title,
          summary: jsonHomeWelcome.summary,
        };
        this.fbWidth = this.isDesktop ? 800 : 280;
        this.fbHeight = this.isDesktop ? 411 : 235;


        const videoLink = `https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F${this.homeWelcome.video.pageAccount}%2Fvideos%2F${this.homeWelcome.video.ID}%2F&width=800&show_text=false&height=411&appId`;
        this.homeVideoSrc = this.sanitizer.bypassSecurityTrustResourceUrl(videoLink);
        const coverPhotoLink = this.imageSrc + this.homeWelcome.coverPhoto.fileName;
        this.coverPhotoSrc = this.sanitizer.bypassSecurityTrustResourceUrl(coverPhotoLink);


      });

    this.homeService.getHomeFarms()
      .subscribe((jsonHomeFarms: IHomeFarm) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonHomeFarms.summary[0]);
        this.homeFarmsSummary.nativeElement.innerHTML = quill.root.innerHTML;

        this.homeFarms = {
          farms: jsonHomeFarms.farms,
          title: jsonHomeFarms.title,
          summary: jsonHomeFarms.summary,
        };
      });

    this.homeService.getHomeProducts()
      .subscribe((jsonHomeProducts: IHomeProduct) => {
        const quill = this.quillService.instantiate();
        quill.setContents(jsonHomeProducts.summary[0]);
        this.homeProductsSummary.nativeElement.innerHTML = quill.root.innerHTML;

        this.homeProducts = {
          brands: jsonHomeProducts.brands,
          title: jsonHomeProducts.title,
          summary: jsonHomeProducts.summary,
        };
      });
  }
}
