﻿const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const cors = require('cors');
const fs = require('fs');

const port = process.env.PORT || 3000;

const app = express();

app.use(cors());

app.use(morgan('dev'));

// Cookies
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//require('./app/auth')(app, passport);
require('./app/routes')(app);

// Create upload directory
const dir = './uploads';
if (!fs.existsSync(dir)) {
	console.log('create directory');
	fs.mkdirSync(dir);
}

app.listen(port, function () {
	console.log('Server started on port: ' + port);
});

module.exports = app;
